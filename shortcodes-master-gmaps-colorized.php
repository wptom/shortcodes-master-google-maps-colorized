<?php
/*
  Plugin Name: Shortcodes Master - GMaps Colorized
  Plugin URI: https://lizatom.com/wordpress-google-maps-plugin
  Version: 1.0.0
  Author: Lizatom.com
  Author URI: https://lizatom.com
  Description: Colorize your Google maps with this Shortcodes Master addon.
  Text Domain: smgmc
  Domain Path: /lang
  License: license.txt
 */

define( 'SMGMC_PLUGIN_FILE', __FILE__ );
define( 'SMGMC_PLUGIN_VERSION', '1.0.0' );

require_once 'inc/shortcodes.php';
require_once 'inc/gmaps-colorized.php';