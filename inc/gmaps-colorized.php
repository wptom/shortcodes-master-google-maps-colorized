<?php
class Shortcodes_Master_Gmaps_Colorized {

	/**
	 * Constructor
	 */
	function __construct() {
		// Load textdomain
		load_plugin_textdomain( 'smgmc', false, dirname( plugin_basename( SMGMC_PLUGIN_FILE ) ) . '/lang/' );
		// Reset cache on activation
		if ( class_exists( 'Sm_Generator' ) ) register_activation_hook( SMGMC_PLUGIN_FILE, array( 'Sm_Generator', 'reset' ) );
		// Init plugin
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 20 );
		// Make pluin meta translation-ready
		__( 'Lizatom', 'smgmc' );
		__( 'Shortcodes Master - GMaps Colorized', 'smgmc' );
		__( 'Colorize your Google maps with this Shortcodes Master addon.', 'smgmc' );
	}

	/**
	 * Plugin init
	 */
	public static function init() {
		// Check for SM
		if ( !function_exists( 'shortcodes_master' ) ) {
			// Show notice
			add_action( 'admin_notices', array( __CLASS__, 'sm_notice' ) );
			// Break init
			return;
		}
		// Register assets
		add_action( 'init', array( __CLASS__, 'assets' ) );
		// Add new group to Generator
		add_filter( 'sm/data/groups', array( __CLASS__, 'group' ) );
		// Register new shortcodes
		add_filter( 'sm/data/shortcodes', array( __CLASS__, 'data' ) );
		// Add plugin meta links
		add_filter( 'plugin_row_meta', array( __CLASS__, 'meta_links' ), 10, 2 );
	}

	/**
	 * Install SM notice
	 */
	public static function sm_notice() {
		?><div class="updated">
			<p><?php _e( 'Please install and activate latest version of <b>Shortcodes Master</b> to use it\'s addon <b>Shortcodes Master - GMaps Colorized</b>.<br />Deactivate this addon to hide this message.', 'smgmc' ); ?></p>
			<p><a href="http://shortcodesmaster.com" target="_blank" class="button button-primary"><?php _e( 'Install Shortcodes Master', 'smgmc' ); ?> &rarr;</a></p>	
		</div><?php
	}

	public static function assets() {		
		wp_register_script( 'sm-gmaps', 'https://maps.googleapis.com/maps/api/js?sensor=false', false, SMGMC_PLUGIN_VERSION, false );
        wp_enqueue_script('sm-gmaps');
	}

	public static function meta_links( $links, $file ) {
		if ( $file === plugin_basename( SMGMC_PLUGIN_FILE ) ) $links[] = '<a href="http://lizatom.com/forum/" target="_blank">' . __( 'Support', 'smgmc' ) . '</a>';
		return $links;
	}

	/**
	 * Add new group to the Generator
	 */
	public static function group( $groups ) {
		//$groups['newgroup'] = __( 'New group', 'smgmc' );
		return $groups;
	}

	/**
	 * New shortcodes data
	 */
	public static function data( $shortcodes ) {
		$map_icons = array("2_accountancy", "2_arts-crafts", "2_astrology", "2_automotive", "2_bars", "2_birds", "2_books-media", "2_breakfast-n-brunch", "2_business", "2_cake-shop", "2_clothings", "2_clubs", "2_coffee-n-tea", "2_commercial-places", "2_community", "2_computers", "2_concerts", "2_cookbooks", "2_dance-clubs", "2_default", "2_dental", "2_doctors", "2_education", "2_electronics", "2_employment", "2_engineering", "2_entertainment", "2_event", "2_exhibitions", "2_fashion", "2_festivals", "2_financial-services", "2_food", "2_furniture-stores", "2_games", "2_gifts-flowers", "2_government", "2_halloween", "2_health-medical", "2_home-services", "2_hotels", "2_industries", "2_internet", "2_jewelry", "2_jobs", "2_karaoke", "2_law", "2_lawn-garden", "2_libraries", "2_local-services", "2_lounges", "2_magazines", "2_manufacturing", "2_marker-new1_12", "2_massage-therapy", "2_mass-media", "2_matrimonial", "2_medical", "2_meetups", "2_miscellaneous-for-sale", "2_mobile-phones", "2_movies", "2_museums", "2_musical", "2_musical-instruments", "2_nightlife", "2_parks", "2_parties", "2_pets", "2_photography", "2_pizza", "2_places", "2_playgrounds", "2_play-schools", "2_pool-halls", "2_printing-graphic-arts", "2_professional", "2_real-estate", "2_religious-organizations", "2_residential-places", "2_restaurants", "2_retail-stores", 
			"2_saloon", "2_schools", "2_science", "2_shopping", "2_sporting-goods", "2_sports", "2_swimming-pools", "2_telemarketing", "2_tickets", "2_tiffin-services", "2_tires-accessories", "2_tools-hardware", "2_tours", "2_toys-store", "2_transport", "2_travel", "2_tutors", "2_vacant-land", "accident", "administration", "aestheticscenter", "agriculture", "agriculture2", "agriculture3", "agriculture4", "aircraft-small", "airplane-sport", "airplane-tourism", "airport", "airport-apron", "airport-runway", "airport-terminal", "amphitheater", "amphitheater-tourism", "ancientmonument", "ancienttemple", "ancienttempleruin", "animals", "anniversary", "apartment", "aquarium", "arch", "archery", "artgallery", "atm", "atv", "audio", "australianfootball", "bags", "bank", "bankeuro", "bankpound", "bar", "baseball", "basketball", "baskteball2", "beach", "beautiful", "bench", "bicycleparking", "bigcity", "billiard", "bobsleigh", "bomb", "bookstore", "bowling", "boxing", "bread", "bridge", "bridgemodern", "bullfight", "bungalow", "bus", "butcher", "cabin", "cablecar", "camping", "campingsite", "canoe", "car", "carrental", "carrepair", "carwash", "casino", "castle", "cathedral", "cathedral2", "cave", "cemetary", "chapel", "church", "church2", "cinema", "circus", "citysquare", "climbing", "clothes", "clothes-female", "clothes-male", "clouds", "cloudsun", "club", "cluster", "cluster2", "cluster3", "cluster4", "cluster5", "cocktail", "coffee", "communitycentre", "company", 
			"computer", "concessionaire", "conference", "construction", "convenience", "convent", "corral", "country", "court", "cricket", "cross", "crossingguard", "cruise", "currencyexchange", "customs", "cycling", "cyclingfeedarea", "cyclingmountain1", "cyclingmountain2", "cyclingmountain3", "cyclingmountain4", "cyclingmountainnotrated", "cyclingsport", "cyclingsprint", "cyclinguncategorized", "dam", "dancinghall", "dates", "daycare", "days-dim", "days-dom", "days-jeu", "days-jue", "days-lun", "days-mar", "days-mer", "days-mie", "days-qua", "days-qui", "days-sab", "days-sam", "days-seg", "days-sex", "days-ter", "days-ven", "days-vie", "dentist", "deptstore", "disability", "disabledparking", "diving", "doctor", "dog-leash", "dog-offleash", "door", "down", "downleft", "downright", "downthenleft", "downthenright", "drinkingfountain", "drinkingwater", "drugs", "elevator", "embassy", "entrance", "escalator-down", "escalator-up", "exit", "expert", "explosion", "factory", "fallingrocks", "family", "farm", "fastfood", "festival", "findajob", "findjob", "fire", "fire-extinguisher", "firemen", "fireworks", "firstaid", "fishing", "fishingshop", "fitnesscenter", "fjord", "flood", "flowers", "followpath", "foodtruck", "forest", "fortress", "fossils", "fountain", "friday", "friends", "garden", "gateswalls", "gazstation", "geyser", "gifts", "girlfriend", "glacier", "golf", "gondola", "gourmet", "grocery", "gun", "gym", "hairsalon", "handball", "hanggliding", "hats", "headstone", "headstonejewish", 
			"helicopter", "highway", "hiking", "hiking-tourism", "historicalquarter", "home", "horseriding", "hospital", "hostel", "hotairballoon", "hotel", "hotel1star", "hotel2stars", "hotel3stars", "hotel4stars", "hotel5stars", "hunting", "icecream", "icehockey", "iceskating", "info", "jewelry", "jewishquarter", "jogging", "judo", "justice", "karate", "karting", "kayak", "laboratory", "lake", "laundromat", "left", "leftthendown", "leftthenup", "library", "lighthouse", "liquor", "lock", "lockerrental", "magicshow", "mainroad", "massage", "military", "mine", "mobilephonetower", "modernmonument", "moderntower", "monastery", "monday", "monument", "mosque", "motorbike", "motorcycle", "movierental", "museum", "museum-archeological", "museum-art", "museum-crafts", "museum-historical", "museum-naval", "museum-science", "museum-war", "music", "music-classical", "music-hiphop", "music-live", "music-rock", "nanny", "newsagent", "nordicski", "nursery", "observatory", "oilpumpjack", "olympicsite", "ophthalmologist", "pagoda", "paint", "palace", "panoramic", "panoramic180", "park", "parkandride", 
			"parking", "park-urban", "party", "patisserie", "pedestriancrossing", "pens", "perfumery", "personalwatercraft", "petroglyphs", "pets", "phones", "photo", "photodown", "photodownleft", "photodownright", "photography", "photoleft", "photoright", "photoup", "photoupleft", "photoupright", "picnic", "pizza", "places-unvisited", "places-visited", "planecrash", "playground", "poker", "police", "police2", "pool", "pool-indoor", "port", "postal", "powerlinepole", "powerplant", "powersubstation", "prison", "publicart", "racing", "radiation", "rain", "rattlesnake", "realestate", "recycle", "regroup", "resort", "restaurant", "restaurantafrican", "restaurant-barbecue", "restaurant-buffet", "restaurantchinese", "restaurant-fish", "restaurantfishchips", "restaurantgourmet", "restaurantgreek", "restaurantindian", "restaurantitalian", "restaurantjapanese", "restaurantkebab", "restaurantkorean", "restaurantmediterranean", "restaurantmexican", "restaurant-romantic", 
			"restaurantthai", "restaurantturkish", "revolution", "right", "rightthendown", "rightthenup", "riparian", "ropescourse", "rowboat", "rugby", "ruins", "sailboat", "sailboat-sport", "sailboat-tourism", "satursday", "sauna", "school", "schrink", "sciencecenter", "seals", "seniorsite", "shelter-picnic", "shelter-sleeping", "shoes", "shoppingmall", "shore", "shower", "sight", "skateboarding", "skiing", "skijump", "skilift", "smallcity", "smokingarea", "sneakers", "snow", "snowboarding", "snowmobiling", "snowshoeing", "soccer", "soccer2", "spaceport", "speed100", "speed110", "speed120", "speed130", "speed20", "speed30", "speed40", "speed50", "speed60", "speed70", "speed80", "speed90", "speedhump", "spelunking", "stadium", "statue", "steamtrain", "stop", "stoplight", "strike", "strike1", "subway", "sun", "sunday", "supermarket", "surfing", "suv", 
			"synagogue", "tailor", "tapas", "taxi", "taxiway", "teahouse", "telephone", "templehindu", "tennis", "tennis2", "tent", "terrace", "text", "textiles",
			"theater", "themepark", "thunder", "thursday", "toilets", "tollstation", "tools", "tower", "toys", "trafficenforcementcamera", "train", "tram", "trash", "truck",
			"tuesday", "tunnel", "turnleft", "turnright", "university", "up", "upleft", "upright", "upthenleft", "upthenright", "usfootball", "vespa", "vet", "video",
			"videogames", "villa", "villa-tourism", "waitingroom", "water", "waterfall", "watermill", "waterpark", "waterskiing", "watertower", "waterwell",
			"waterwellpump", "wedding", "wednesday", "wetland", "white1", "white20", "wifi", "windmill", "windsurfing", "windturbine", "winery", "wineyard", "workoffice", "world", "worldheritagesite", "yoga", "youthhostel", "zipline", "zoo");

		$shortcodes['gmap_colorized'] = array(
			'name'     => __( 'Gmaps colorized', 'smgmc' ),
			'type'     => 'single',
			'group'    => 'media',
			'desc'     => __( 'Colorize your Google maps.', 'smgmc' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-color-google-maps/" target="_blank">', '&rarr;</a>' ),
			'icon'     => 'map-marker',
			'function' => array( 'Shortcodes_Master_Gmaps_Colorized_Shortcodes', 'gmap_colorized' ),
			'atts'     => array(
			    'id' => array(
							'values' => array( ),
							'default' => 'mapUniqueId',
							'name' => __( 'Unique ID', 'smgmc' ),
							'desc' => __( 'ID of the map (required).', 'smgmc' )
						    ),
                'url' => array(
                            'values' => array( ),
                            'default' => '',
                            'name' => __( 'URL', 'smgmc' ),
                            'desc' => sprintf( '%s. <a href="https://lizatom.com/wiki/shortcodes-master-color-google-maps/#shortcodes-master-color-google-maps-how-to" target="_blank">%s</a>', __( 'Insert URL of the map', 'smgmc' ), __( 'More info.', 'smgmc' ) )
                 ),            
				'width' => array(
							'type' => 'slider',
							'min' => 200,
							'max' => 1600,
							'step' => 5,
							'default' => 540,
							'name' => __( 'Width', 'smgmc' ),
							'desc' => __( 'Map width', 'smgmc' )
						    ),
				'height' => array(
							'type' => 'slider',
							'min' => 200,
							'max' => 1600,
							'step' => 5,
							'default' => 360,
							'name' => __( 'Height', 'smgmc' ),
							'desc' => __( 'Map height', 'smgmc' )
				            ),				 
				 'icon' => array(
                            'type' => 'image',
                            'default' => 'image: 2_residential-places',
                            'name' => __( 'Marker', 'smgmc' ),
                            'values' => $map_icons,
                            'path_small' => plugins_url( 'assets/images/icons/', SMGMC_PLUGIN_FILE ),
                            'path_big' => plugins_url( 'assets/images/icons/', SMGMC_PLUGIN_FILE ),
                            'mime' => 'png',
                            'desc' => __( 'Pick a marker icon', 'smgmc' )
						),
				'title' => array(
							'default' => '',
							'name' => __( 'Title', 'smgmc' ),
							'desc' => __( 'Title displayed above the marker when moving mouse over it.', 'smgmc' )
						    ),		
				 'style' => array(
							'type' => 'select',
							'values' => array(
								'Pale Dawn' => __( 'Pale Dawn', 'smgmc' ),
								'Subtle Grayscale' => __( 'Subtle Grayscale', 'smgmc' ),
								'Blue Water' => __( 'Blue Water', 'smgmc' ),
                                'Midnight Commander' => __( 'Midnight Commander', 'smgmc' ),
                                'Retro' => __( 'Retro', 'smgmc' ),
                                'Shades of gray' => __( 'Shades of gray', 'smgmc' ),
                                'Grayscale' => __( 'Grayscale', 'smgmc' ),
                                'Light monochrome' => __( 'Light monochrome', 'smgmc' ),
                                'Bright and Bubbly' => __( 'Bright and Bubbly', 'smgmc' ),
                                'Icy Blue' => __( 'Icy Blue', 'smgmc' ),
                                'Red alert' => __( 'Red alert', 'smgmc' ),
                                'Turquoise' => __( 'Turquoise', 'smgmc' ),
                                'Flat green' => __( 'Flat green', 'smgmc' ),
                                'Vitamin C' => __( 'Vitamin C', 'smgmc' ),
                                'Hard edges' => __( 'Hard edges', 'smgmc' ),
                                'Yellow-green' => __( 'Yellow-green', 'smgmc' ),
                                'Night vision' => __( 'Night vision', 'smgmc' ),
                                'Blue print' => __( 'Blue print', 'smgmc' ),
                                'Dark' => __( 'Dark', 'smgmc' )
							),
							'default' => 'Pale Dawn',
							'name' => __( 'Style', 'smgmc' ),
							'desc' => __( 'Style of the map.', 'smgmc' )
						),		               				
			)
		);

		return $shortcodes;
	}
}

new Shortcodes_Master_Gmaps_Colorized;
